### install fluxcd
1. export=<GITLAB_TOKEN>
2. flux bootstrap gitlab \
    --owner=<USERNAME> \
    --repository=<REPOSITORY> \
    --branch=<BRANCH> \
    --path=<PATH/TO/CLUSTER> \
    --token-auth

### generate source and resource
1. flux create source git <APP_NAME> \
    --url=<REPOSITORY_URL> \
    --branch=<BRANCH> \
    --interval=30s \
    --export > ./clusters/test/app-source.yaml
2. flux create kustomization <APP_NAME> \
    --target-namespace=default \
    --source=<APP_SOURCE> \
    --path="<PATH/TO/FILE>" \
    --prune=true \
    --interval=5m \
    --export > ./clusters/test/app-kustomization.yaml